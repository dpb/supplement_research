## Prevagen and Apoaequorin Links

### Nature and characteristics

 * Apoaequorin is part of the chemical process that makes a certain [hydrozoan jellyfish](https://www.google.com/search?q=Aequorea+victoria&tbm=isch) glow. The [Wikipedia article](https://en.wikipedia.org/wiki/Aequorin) (accessed 20160102) says:
 
   > Apoaequorin is the enzyme produced in the photocytes of the animal, and coelenterazine is the substrate whose oxidation the enzyme catalyzes.
   
   The 2008 Nobel Prize in Chemistry was awarded for research on these processes, but not in connection with the chemical's use as a health supplement (marketed as Prevagen).
   
   There is a popular summary from 20000524 [here](http://www.sciencedaily.com/releases/2000/05/000522082947.htm) about the utility of aequorin for tracking the movement of calcium within cells, which has implications for research on memory and cognition.

### Research Reports

 * Moran et al. (2014), "[Safety assessment of the calcium-binding protein, apoaequorin, expressed by Escherichia coli.](http://www.ncbi.nlm.nih.gov/pubmed/24768935)." (Two of the four authors work for Quincy Bioscience, which markets Prevagen.)
 
   > **Abstract**
   > 
   > Calcium-binding proteins are ubiquitous modulators of cellular activity and function. Cells possess numerous calcium-binding proteins that regulate calcium concentration in the cytosol by buffering excess free calcium ion. Disturbances in intracellular calcium homeostasis are at the heart of many age-related conditions making these proteins targets for therapeutic intervention. A calcium-binding protein, apoaequorin, has shown potential utility in a broad spectrum of applications for human health and well-being. Large-scale recombinant production of the protein has been successful; enabling further research and development and commercialization efforts. Previous work reported a 90-day subchronic toxicity test that demonstrated this protein has no toxicity by oral exposure in Sprague-Dawley rodents. The current study assesses the allergenic potential of the purified protein using bioinformatic analysis and simulated gastric digestion. The results from the bioinformatics searches with the apoaequorin sequence show the protein is not a known allergen and not likely to cross-react with known allergens. Apoaequorin is easily digested by pepsin, a characteristic commonly exhibited by many non-allergenic dietary proteins. From these data, there is no added concern of safety due to unusual stability of the protein by ingestion.
   >
   > [**Highlights**](http://www.sciencedirect.com/science/article/pii/S0273230014000671)
   >
   > * A recombinant protein, apoaequorin, has been produced and is novel to the human diet.
   > * U.S. and international regulatory agencies require safety assessments of the protein.
   > * We assessed the allergenicity of the protein sequence against allergen databases.
   > * We tested simulated pepsin digestion of the protein.
   > * We conclude that this protein does not pose an allergenic threat to the public.

 * Deter et al. (2013), "[Pretreatment with apoaequorin protects hippocampal CA1 neurons from oxygen-glucose deprivation](http://www.ncbi.nlm.nih.gov/pubmed/24244400)." [Full-text here](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3823939/).

   > **Abstract**
   > 
   > Ischemic stroke affects ∼795,000 people each year in the U.S., which results in an estimated annual cost of $73.7 billion. Calcium is pivotal in a variety of neuronal signaling cascades, however, during ischemia, excess calcium influx can trigger excitotoxic cell death. Calcium binding proteins help neurons regulate/buffer intracellular calcium levels during ischemia. Aequorin is a calcium binding protein isolated from the jellyfish Aequorea victoria, and has been used for years as a calcium indicator, but little is known about its neuroprotective properties. The present study used an in vitro rat brain slice preparation to test the hypothesis that an intra-hippocampal infusion of apoaequorin (the calcium binding component of aequorin) protects neurons from ischemic cell death. Bilaterally cannulated rats received an apoaequorin infusion in one hemisphere and vehicle control in the other. Hippocampal slices were then prepared and subjected to 5 minutes of oxygen-glucose deprivation (OGD), and cell death was assayed by trypan blue exclusion. Apoaequorin dose-dependently protected neurons from OGD--doses of 1% and 4% (but not 0.4%) significantly decreased the number of trypan blue-labeled neurons. This effect was also time dependent, lasting up to 48 hours. This time dependent effect was paralleled by changes in cytokine and chemokine expression, indicating that apoaequorin may protect neurons via a neuroimmunomodulatory mechanism. These data support the hypothesis that pretreatment with apoaequorin protects neurons against ischemic cell death, and may be an effective neurotherapeutic.

 * Moran et al. (2013) "[Safety assessment of Apoaequorin, a protein preparation: subchronic toxicity study in rats.](http://www.ncbi.nlm.nih.gov/pubmed/23470325)." (Main author works for Quincy Bioscience, which markets Prevagen.)
 
   > **Abstract**
   > 
   > Apoaequorin, a calcium-binding protein originally isolated from jellyfish is available commercially as a dietary supplement. The objective of the present study was to investigate potential adverse effects, if any, of Apoaequorin, a recombinant protein preparation, in rats following subchronic administration. For this study, Sprague-Dawley (Hsd:SD) rats (10/sex/group) were administered via oral gavage 0 (control), 92.6, 462.9, and 926.0mg/kg body weight (bw)/day of Apoaequorin preparation, for 90 days. The corresponding amount of Apoaequorin protein was 0, 66.7, 333.3 and 666.7 mg/kg bw/day, respectively. Administration of the Apoaequorin preparation did not result in any mortality. There were no clinical or ophthalmological signs, body weight, body weight gain, food consumption, food efficiency, clinical pathology or histopathological changes attributable to administration of Apoaequorin. Any changes noted were incidental and in agreement with those historically observed in the age and strain of rats used in this study. Based on the results of this study, the No Observed-Adverse-Effect Level (NOAEL) for Apoaequorin was determined as 666.7 mg/kg bw/day, the highest dose tested.
   > 
   > [**Highlights**](http://www.sciencedirect.com/science/article/pii/S0278691513001567)
   >
   > * Subchronic toxicity of Apoaequorin, a protein preparation was investigated in rats. 
   > * Apoaequorin administration to rats at doses up to 667 mg/kg/day for 90 days did not reveal any adverse effects. 
   > * The No Observed-Adverse-Effect Level (NOAEL) for Apoaequorin was determined as 667 mg/kg bw/day, the highest dose tested.

#### Other:

   * Search for research reports on the U.S. National Center for Biotechnology Information:
   
     * [**Apoaequorin or Prevagen and stroke**](http://goo.gl/Sb0Xje)
   
       `http://www.ncbi.nlm.nih.gov/pubmed?term=(Apoaequorin+OR+Prevagen)+AND+stroke`
     
     * [**Apoaequorin or Prevagen and toxicity**](http://goo.gl/yqoB3U)
   
       `http://www.ncbi.nlm.nih.gov/pubmed/?term=(Apoaequorin+OR+Prevagen)+AND+toxicity`

   * (no date; perhaps regularly updated?): [Prevagen's own research page](https://www.prevagen.com/research/)
 
   * 201501 or earlier (undated, but first comment is 20150111): [Supplement Geek](http://supplement-geek.com/prevagen-review/). Some remarks on Prevagen's official research page by Joe Cannon.

---

### Lawsuits about Prevagen

 * 20150121: [Phillip Racies v. Quincy Bioscience, LLC](http://www.casewatch.org/civil/quincy/complaint_2015.pdf)
 
   * Comments:
   
     * 20150908: Heather Sawitsky, [White Oak Cottages](http://whiteoakcottages.com/prevagen-buyer-beware/) (an assisted-living residence for people with dementia or Alzheimer’s disease, Boston area)
     
       > The lawsuit claims that the active ingredient in Prevagen is completely broken down during the digestive process and has no effect on brain function, making the marketing false and misleading.  The complaint also challenges the veracity of the clinical trial results posted by QB.  The suit is currently in the discovery phase, but it has withstood motions to  have the case dismissed.

     * 201504: Law firm [Janet, Jenner and Suggs, LLC](http://myadvocates.com/blog/dangerous-drugs-devices/prevagen-fda-investigation) summarizes the lawsuit:
     
       > In January, 2015, a class-action suit was against Quincy Bioscience (QB) LLC the manufacturer of Prevagen. The company claims that the product works by re-supplying memory-related proteins that decline as people age. The lawsuit charges:

       > • The product cannot work as advertised because its only purported active ingredient, apoaequorin (a protein), is completely destroyed by the digestive system and transformed into common amino acids no different than those derived from other common food products.

       > • The amount of amino acids Prevagen adds to the user’s intake are trivial in comparison to normal dietary intake.

       > • Claims that clinical tests demonstrate that Prevagen will improve memory and support healthy brain function, sharper mind, and clearer thinking are false. (Note: QB’s “clinical trials” were conducted in-house and did not have independent verification.)

       > • Studies touted in Prevagen’s marketing campaign “if they exist at all, are, on their face, so seriously flawed that they demonstrate nothing regarding Prevagen.”

### Federal Regulation

 * [FDA Warning](http://www.fda.gov/ICECI/EnforcementActions/WarningLetters/2012/ucm324557.htm)
 
 * 20121024: [Natural Products Insider](http://www.naturalproductsinsider.com/articles/2012/10/quincy-bioscience-s-brain-vitamin-suffers-regulat.aspx):
 
   > Reports of chest pain, tremors, fainting and much worse symptoms have been associated with the vitamin. In a recent warning letter, the FDA pointed out Quincy Bioscience neglected to tell it about "adverse events like seizures, strokes, and worsening symptoms of multiple sclerosis that had been reported to your firm as being associated with the use of Prevagen products."
   >
   > In fact, of more than 1,000 incidents and product complaints reported to Quincy Bioscience between May 2008 and December 1, 2011, the Madison, Wis.-based biotechnology company had only investigated or reported two events at the time government regulators inspected its facility at the end of the year, according to the warning letter. 
   >
   > ...
   >
   > "Apoaequorin synthetically produced from (b)(4) is not a vitamin, mineral, amino acid, herb or other botanical, or dietary substance for use by man to supplement the diet by increasing the total dietary intake. Further, apoaequorin synthetically produced from (b)(4) is not a concentrate, metabolite, constituent, or extract of any dietary ingredient, nor is it a combination of dietary ingredients," FDA explained in the warning letter. "Therefore, the synthetically produced apoaequorin used in your Prevagen products is not a dietary ingredient as defined" under federal law.
   > 
   > The company, Ullman said, "may have in effect frozen themselves out of the supplement market."
   > 
   > Quincy Bioscience has even more explaining to do. On a website www.hopetrials.com, Quincy Bioscience has described clinical trials that were conducted to study apoaequorin for use in treating or preventing diseases, according to FDA. That wouldn't be a problem except for one fact: doing so without an investigational drug application or so-called INE violates federal law, the agency said. And despite promising corrective action earlier this year, the agency wrote that Quincy Bioscience continues to use apoaquorin in clinical trials without the proper application.
   > 
   > Another 2011 inspection of the company's facilities in Middleton, Wis. discovered a number of violations of current good manufacturing practice regulations. FDA acknowledged Quincy Bioscience has taken some remedial action but the agency indicated its response was inadequate because FDA has determined the Prevagen products are drugs rather than dietary supplements.
   > 
   > On its website, Quincy Bioscience asserts its "Quality Assurance Department is fully compliant with current Good Manufacturing Practice (cGMP) standards as defined in federal code by 21 CFR 111." FDA obviously has adopted a different position. 

 * 20131205: Madison, Wisconsin-based [Isthmus](http://isthmus.com/news/news/the-fda-looks-into-quincy-biosciences-claims-for-prevagen/):
 
   > Fourteen months later, the FDA still lists the Quincy case as "open." According to FDA records secured through a Freedom of Information request, Quincy has reported a series of wide-ranging steps taken to comply with FDA demands, including scrubbing the suspect claims, videos and links from its online profile, retraining its employees on FDA supplement regulations and improving its internal record keeping.
   > 
   > ...
   > 
   > The FDA declined repeated requests to clarify the status of the Quincy warning. Underwood correctly points out that the "open" designation is not unusual. Of the 72 FDA warning letters issued in October 2012, only 16 are listed as closed.
   > 
   > ...
   >
   > Alzheimer's, however, is not a disease of age, according to Dr. Mark Sager, who is the principal investigator of the Wisconsin Registry for Alzheimer's Prevention. This well-regarded longitudinal study of adult children of parents with Alzheimer's is trying to detect the early markers of the disease.
   >
   > "People think Alzheimer's is a disease of aging," he says. "But it's a disease of a lifetime that only becomes evident in older adults."
   >
   > Sager says Alzheimer's manifests itself for both genetic and environmental reasons, much as a person's genetic propensity for heart disease can be exacerbated by smoking and lack of exercise.
   >
   > "We find that diet, exercise and lifestyle -- stress, for example -- are all associated with Alzheimer's," says Sager. "If you're overweight and don't exercise in midlife, you have an increased risk of developing Alzheimer's later on. "
   >
   > Sager, who runs the Wisconsin Alzheimer's Institute, says it does not recommend taking Prevagen "primarily because there's no evidence it does any good." He suggests people worried about Alzheimer's would be better off adopting the Mediterranean diet, which he says has proven benefits.
   >
   > Carlsson, who runs a memory clinic at the Madison Veterans Hospital, says the various brain supplements sold over the counter probably won't hurt anyone, "but some do have side effects. As to their claims they can help you, I can't validate that, whether it's Prevagen, coconut oil, Coenzyme Q10. We just don't have the answers."
   >
   > Her advice: "Instead of taking Prevagen, I'd encourage people to get a membership at a health club."
   >
   > Far more scathing in his criticism is UW-Madison neuroscientist Baron Chanda. "This product doesn't make sense. It's basically quackery," he says after reviewing some of the online Prevagen research. He says there is no way the apoaequorin protein could survive the digestive tract and make its way to the brain. Prevagen purchasers, he suggests, are basically being played for "suckers."

[end]